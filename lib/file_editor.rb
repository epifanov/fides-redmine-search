class FileEditor

  def edit_config(option, value)
    File.open('./lib/config.rb', 'r+') do |f|   
      out = ""
      f.each do |line|
        if line.include?('SORT_DIRECTION') && option == 'sort_dir'
          out << "SORT_DIRECTION = '#{value}'"
        elsif line.include?('SORT_BY') && option == 'sort_by' && AVALIBLE_COLUMNS.include?(value)
          out << "SORT_BY = '#{value}'\n"
        elsif line.include?('DEFAULT_COLUMNS') && option == 'def_col_add' && !line.include?(value) && AVALIBLE_COLUMNS.include?(value)
          out << line.sub('nil', "'#{value}'")
        elsif line.include?('DEFAULT_COLUMNS') && option == 'def_col_rm'
          out << line.sub("'#{value}'", 'nil')
        else
          out << line
        end
      end
      f.pos = 0                     
      f.print out
      f.truncate(f.pos)
    end
  end
end