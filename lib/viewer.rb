class Viewer
  def initialize(issues_array)
    @rows = show_result(issues_array)
  end

  def to_console
    @rows.each { |row| puts row }
  end

  def to_file(filename)
    File.open(filename, "w+") do |f|
      @rows.each { |element| f.puts(delete_colors(element)) }
    end
  end

  private

  def sort_array(array)
    array.sort_by! { |issue| SORT_BY == 'id' ? issue[SORT_BY].to_i : issue[SORT_BY] }
    if SORT_DIRECTION == 'asc'
      array
    elsif SORT_DIRECTION == 'desc'
      array.reverse
    end
  end

  def show_result(array)
    flag = array.last['comments'].nil?
    array = array.each {|i| i['comments'] = i['comments'].map {|c| c['text']}.join(COMMENT_SEPARATOR) } unless flag
    result = sort_array(array)
    rows = []
    columns = DEFAULT_COLUMNS.compact
    comment_spaces = 0
    comment_length = 0

    spaces = columns.map do |c|
      if c == 'comments'
        comment_length = result.map {|issue| oneline_comments(issue[c]).map {|l| l.size }.max.to_i}.max.to_i
        comment_length > 0 ?  comment_length + c.length : c.length + 5
      else
        result.map { |i| i[c].length == 0 ? c.length + 5 : i[c].length + 5 }.max
      end
    end

    rows.push('')
    columns.each do |c|
      comment_spaces = rows[-1].size if c == 'comments'
      rows[-1] += make_spaces(c, spaces[columns.find_index(c)])
    end

    result.each do |issue|
      rows.push('')
      current_row = rows.find_index(rows[-1])
      columns.each do |c|
        if c == 'comments'
          comments = oneline_comments(issue[c])
          rows[current_row] += make_spaces(issue[c], spaces[columns.find_index(c)]) if comments.empty?
          comments.each_with_index do |new_line, index|
            if index == 0
              rows[current_row] += make_spaces(new_line, spaces[columns.find_index(c)])
            else
              rows <<  " "*comment_spaces+new_line
            end
          end
        else
          rows[current_row] += make_spaces(issue[c], spaces[columns.find_index(c)])
        end
      end
    end
    rows
  end

  def make_spaces(value, size)
    value.gsub!(COMMENT_SEPARATOR, '')
    value + " "*(size - value.size)
  end

  def oneline_comments(string)
    s = COMMENT_SEPARATOR
    string = string.split(s).each_with_index do |v, k|
      v = v.prepend("##{k+1}: ")
    end.join(s)
    string.gsub(s, "\n").split("\n")
  end

  def delete_colors(string)
    string.gsub("\e[32m", '').gsub("\e[0m", '')
  end
end