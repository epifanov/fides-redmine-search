class IssuesGrabber
  def initialize(date=nil, slow_grab=false)
    @date = date
    @slow_grab = slow_grab
    @headers = { 'X-Redmine-API-Key' => API_KEY }
    @cache_file = './tmp/cache'
    @comments_check = './tmp/comments_check'
  end

  def grab(with_comments=false)
    if with_comments
      @date.nil? && check_usable_cache && exist_comment_cache ? read_cache : get_comments(get_issues)
    else
      check_usable_cache ? read_cache : get_issues
    end
  end

  private

  def check_usable_cache
    File.exist?(@cache_file) && (Time.now - File.mtime(@cache_file))/60.0 < CACHE_TIME && !File.zero?(@cache_file)
  end

  def read_cache
    result = ''
    File.open(@cache_file, 'r') do |f|
      result = f.read
    end
    eval Zlib.inflate(result).force_encoding('utf-8')
  end

  def get_issues
    puts "Getting issues..."
    array = []
    total_pages = 0
    pages = 1

    if @date && !@slow_grab
      xml = get_xml(pages, :created)
      updated_issues = get_xml(pages, :updated)
      issues_size = xml.xpath('issues/issue').size
      issues_size > 0 ? xml.at('issues').add_child(updated_issues.search('issue')) : xml = updated_issues
      array += parse_xml(xml)
    else
      begin
        xml = get_xml(pages)
        if total_pages == 0
          issues_count = xml.xpath('issues').attr('total_count').text.to_i
          total_pages = (issues_count*1.0/LIMIT.to_i).ceil
        end

        printf((pages*100.0/total_pages).round().to_s + "%")
        printf("\r")
        array += parse_xml(xml)
        pages += 1
      end while pages <= total_pages
    end
    printf("\n")

    comments_cache_uncheck
    make_cache(array)
    clear_cache if @date
    array.uniq
  end

  def get_comments(array)
    puts "Getting comments..."
    array.each_with_index do |issue, index|
      uri = URI("#{URL}issues/#{issue['id']}.xml?include=journals")
      response = Net::HTTP.start(uri.host, uri.port) do |http|
        http.get(uri.request_uri, @headers)
      end
      xml = Nokogiri::XML(response.body)
      issue['comments'] = []
      xml.xpath('//journals/journal').each do |comment|
        user = comment.xpath('user').attr('name').text
        note = comment.xpath('notes').text
        date = comment.xpath('created_on').text
        issue['comments'] << { 'text' => note, 'comments_created_on' => Date.parse(date).to_s, 'comments_author' => user}
      end
      printf(((index+1)*100.0/array.size).round().to_s + "%")
      printf("\r")
    end
    printf("\n")

    make_cache(array)
    clear_cache if @date
    comments_cache_check
    array
  end

  private

  def get_xml(pages, date_flag=nil)
    params =  if date_flag == :created
      "&created_on=#{@date}"
    elsif date_flag == :updated
      "&updated_on=#{@date}"
    else
      ''
    end
    uri = URI("#{URL}issues.xml?limit=#{LIMIT}&page=#{pages}&status_id=*#{params}")
    response = Net::HTTP.start(uri.host, uri.port) do |http|
      http.get(uri.request_uri, @headers)
    end
    Nokogiri::XML(response.body)
  end

  def parse_xml(xml)
    array = []
    xml.xpath('issues/issue').each do |issue|
      hash = {}

      hash['id'] = issue.xpath('id').text
      hash['subject'] = issue.xpath('subject').text
      hash['author'] = issue.xpath('author').attr('name').text
      hash['created_on'] = show_time(Time.parse(issue.xpath('created_on').text).to_s)
      hash['status'] = issue.xpath('status').attr('name').text
      assignee = issue.xpath('assigned_to')
      hash['assigned_to'] = assignee.empty? ? '' : assignee.attr('name').text
      hash['priority'] = issue.xpath('priority').attr('name').text
      hash['description'] = issue.xpath('description').text

      array << hash
    end
    array
  end

  def show_time(time)
    ENV['TZ']='UTC'
    secs=Time.strptime(time,"%Y-%m-%d %H:%M:%S").to_i
    ENV['TZ']='Europe/Moscow'
    Time::at(secs).to_s
  end

  def make_cache(array)
    File.open(@cache_file, "w+") do |f|
      f.puts(Zlib.deflate(array.to_s))
    end
  end

  def comments_cache_check
    File.open(@comments_check, "w+") do |f|
      f.puts(1)
    end
  end

  def comments_cache_uncheck
    File.delete(@comments_check) if exist_comment_cache
  end

  def exist_comment_cache
    File.exist?(@comments_check)
  end

  def clear_cache
    File.delete(@cache_file) if File.exist?(@cache_file)
  end
end