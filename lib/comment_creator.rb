class CommentCreator
  def initialize(date, array)
    @date = date
    @headers = { 'X-Redmine-API-Key' => API_KEY }  
    @array = array
  end

  def add(issue_id)
    begin
      RestClient::Request.execute(method: :put, url: "#{URL}/issues/#{issue_id}.json", headers: @headers,
                                 payload: { 'issue' => { 'notes' => generate } }, content_type: :json, accept: :json)
      puts "Successfully added comment to issue ##{issue_id}"
    rescue RestClient::ResourceNotFound
      puts "There is No issue with id: #{issue_id}"
      exit
    end
  end

  def self.help
    puts %Q("Daily Task":
      Пример вызова комманды:
      ruby cli.rb --daily-task --number 9 --author 'Redmine Admin' --date 2015-12-28
      --number - Номер тикета, к которому будет добавлен комментарий
      --author - Автор искомого тикета или комментрия
      --date - Дата создания искомого тикета или комментария(Если не указано, то берется текущая дата)
      Важно: при использовании Daily Task функция кэша отключена)
  end

  def generate
    result = ''
    @array.each do |issue|
      description = scan_description(issue['description']).uniq.join(', ')
      comments = uniq_comments scan_comments(issue['comments'])
      if comments.empty?
        result << row(description, issue) unless description.empty?
      else
        comments.each do |c|
          text = [description, c].join(' | ') if !c.empty? || !description.empty?
          if text
            result << row(text, issue)
          end
        end
      end
    end
    local_date + "\n" + (result.empty? ? "1.\n2.\n3." : result)
  end

  def uniq_comments(comments)
    result = []
    comments.each {|c| c.each {|p| result << p }}
    result.uniq
  end

  def row(string, issue)
    ['#', string, '#' + issue['id'].to_s, issue['status']].join(' ') + "\n"
  end

  def scan_comments(comments)
    comments.map do |comment|
      comment["text"].to_s.scan(/#{Regexp.escape(LEFT_SIGN)}(.*?)#{Regexp.escape(RIGHT_SIGN)}/m)
    end.delete_if { |v| v.empty? }
  end

  def scan_description(text)
    text.to_s.scan(/#{Regexp.escape(LEFT_SIGN)}(.*?)#{Regexp.escape(RIGHT_SIGN)}/m)
  end

  def local_date
    begin
      date = Date.parse(@date)
    rescue ArgumentError
      date = Date.today
    end
    date.strftime("%d #{LOCAL_MONTHS[date.month - 1]} %Y")
  end
end
