class Searcher
  def initialize(search_command, filename=nil)
    @search_command = filename.empty? ? search_command : command_from_file(filename)
  end

  def search(date=nil, slow_grab=false)
    result = []
    commands = parse_command(@search_command)
    grabber = IssuesGrabber.new(date, slow_grab)
    check_commands(commands)
    if search_by_comment(commands)
      issues_array = grabber.grab(true)
    else
      issues_array = grabber.grab
    end

    commands.each do |requests|
      sub_result = issues_array
      requests.each do |r|
        if AVALIBLE_COLUMNS.include? r[:param]
          if r[:operator] == '='
            sub_result = equal_search(sub_result, r[:param], r[:value])
          elsif r[:operator] == '!='
            sub_result = not_equal_search(sub_result, r[:param], r[:value])
          elsif r[:operator] == 'LIKE'
            sub_result = like_search(sub_result, r[:param], r[:value])
          elsif (r[:operator] == '>' || r[:operator] == '<') && r[:param] == 'created_on'
            sub_result = date_search(sub_result, r[:operator], r[:value])
          else
            puts "ERROR: Wrong operator '#{r[:operator]}'"
            exit
          end
        else
          puts "ERROR: Wrong field to search '#{r[:param]}'"
          exit
        end
      end
      result = result | sub_result
    end
    result
  end

  private

  def check_commands(commands)
    commands.each do |requests|
      requests.each do |r|
        if AVALIBLE_COLUMNS.include? r[:param]
          unless r[:operator] == '=' || r[:operator] == '!=' || r[:operator] == 'LIKE' ||
                 (r[:operator] == '>' || r[:operator] == '<') && r[:param] == 'created_on'
            puts "ERROR: Wrong operator '#{r[:operator]}'"
            exit
          end
        else
          puts "ERROR: Wrong field to search '#{r[:param]}'"
          exit
        end
      end
    end
  end

  def command_from_file(filename)
    result = ''
    begin
    File.open(filename, "r") do |f|
      result = f.read
    end
    rescue Errno::ENOENT
      puts "ERROR: No such file '#{filename}'"
      exit
    end
    result.gsub("\n", '')
  end

  def parse_command(search_command)
    array = []
    begin
    request_array = search_command.split(/\sOR\s/m).each do |request|
      sub_array = []
      request.split(/\sAND\s/m).each do |command|
        hash = {}
        operator = command[/\s(.*?)\s/m]
        if operator
          hash[:operator] = operator.gsub(/\s/m, '')
          hash[:value] = downcase command[/#{operator}(.*?)$/m].gsub(operator, '')
          hash[:param] = command[/^\w*\s/m].gsub(/\s/m, '')
          sub_array << hash
        end
      end
      array << sub_array unless sub_array.empty?
    end
    rescue NoMethodError
      puts "ERROR: Wrong command format"
      exit
    end
    array
  end

  def equal_search(array, param, value)
    if param == 'created_on'
      array.select {|issue| Date.parse(issue[param]) == Date.parse(value) }
    elsif param == 'comments'
      array.select do |issue|
        issue['comments'].map { |c| downcase(c['text']) == value }.include? true
      end
    elsif param.to_s.include? 'comments_'
      array.select do |issue|
        flag = false
        comments = []
        comments += issue['comments']
        comments.each do |comment|
          downcase(comment[param]) == value ? flag = true : issue['comments'].delete(comment)
        end
        flag
      end
    else
      array.select {|issue| downcase(issue[param]) == value }
    end
  end

  def not_equal_search(array, param, value)
    if param == 'created_on'
      array.select {|issue| Date.parse(issue[param]) != Date.parse(value) }
    elsif param == 'comments'
      array.select do |issue|
        issue['comments'].map { |c| downcase(c['text']) != value }.include? true
      end
    else
      array.select {|issue| downcase(issue[param]) != value }
    end
  end

  def like_search(array, param, value)
    if param == 'comments'
      array.select do |issue|
        flag = false
        next unless issue['comments'].map { |c| downcase(c['text']).include? value }.include? true
        comments = []
        comments += issue['comments']
        comments.each do |comment|
          if downcase(comment['text']).include?(value)
            flag = true
            comment['text'] = truncate(comment['text'], value)
          else
            issue['comments'].delete(comment)
          end
        end
        flag
      end
    else
      array.select {|issue| downcase(issue[param]).include?(value) }
    end
  end

  def date_search(array, operator, value)
    if operator == '>'
      array.select {|issue| Date.parse(issue['created_on']) > Date.parse(value) }
    elsif operator == '<'
      array.select {|issue| Date.parse(issue['created_on']) < Date.parse(value) }
    end
  end

  def search_by_comment(commands)
    in_params = commands.map {|c| c.map {|cm| cm[:param].to_s.include? 'comments' }.include? true }.include? true
    in_params || DEFAULT_COLUMNS.include?('comments')
  end

  def downcase(string)
    string.to_s.mb_chars.downcase.to_s
  end

  def truncate(string, value)
    string = normalize_string(string)
    #index_array = downcase(string).to_enum(:scan, value).map{ |m,| $`.size }
    start_i = downcase(string).index(value)
    end_i = value.size
    value = string[start_i, end_i]
    result =  string[/.{0,#{TRUNCATE_TO}}#{Regexp.escape(value)}.{0,#{TRUNCATE_TO}}/]
    result =  '...' + result if start_i > TRUNCATE_TO
    result =  result + '...' if string.size - start_i - end_i > TRUNCATE_TO
    make_green(result, value)
  end

  def make_green(string, value)
    string.gsub(/#{Regexp.escape(value)}/, "\e[32m#{value}\e[0m")
  end

  def normalize_string(string)
    string.gsub("\n", ' ').gsub("\r", '')
  end
end