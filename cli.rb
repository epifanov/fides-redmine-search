require 'net/http'
require 'uri'
require 'nokogiri'
require 'time'
require 'getoptlong'
require 'zlib'
require 'active_support/core_ext/string'
require 'rest-client'

if `gem list | grep rest-client`.nil?
  puts 'Installing gem rest-client...'
  `gem install rest-client`
end

if `gem list | grep nokogiri`.nil?
  puts 'Installing gem nokogiri...'
  `gem install nokogiri`
end

if `gem list | grep activesupport`.nil?
  puts 'Installing gem activesupport...'
  `gem install activesupport`
end

require './lib/config'
require './lib/searcher'
require './lib/viewer'
require './lib/issues_grabber'
require './lib/file_editor'
require './lib/comment_creator'

opts = GetoptLong.new(
  [ '--sort', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--asc', GetoptLong::NO_ARGUMENT ],
  [ '--desc', GetoptLong::NO_ARGUMENT ],
  [ '--search', GetoptLong::OPTIONAL_ARGUMENT ],
  [ '--add', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--rm', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--to-file', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--from-file', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--daily-task', GetoptLong::NO_ARGUMENT ],
  [ '--date', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--author', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--number', GetoptLong::REQUIRED_ARGUMENT ],
  [ '--help', '-h', GetoptLong::NO_ARGUMENT ]
)

sort_direction = sort_value = search_command = columt_to_add = columt_to_rm = from_file = to_file = author = issue_id = ''
slow_grab = help = search = daily_task = false
date = Date.today.to_s

opts.each do |opt, arg|
  if opt == '--search'
    search = true
    search_command = arg
  end
  sort_value = arg if opt == '--sort'
  if opt == '--asc'
    sort_direction = 'asc'
  elsif opt == '--desc'
    sort_direction = 'desc'
  end
  columt_to_add = arg if opt == '--add'
  columt_to_rm = arg if opt == '--rm'
  to_file = arg if opt == '--to-file'
  from_file = arg if opt == '--from-file'

  daily_task = true if opt == '--daily-task'
  if daily_task
    if opt == '--date'
      date = arg 
      slow_grab = true
    end
    author = arg if opt == '--author'
    issue_id = arg if opt == '--number'
  end
  if (opt == '--help' || opt == '-h') && daily_task
    CommentCreator.help
    help = true
  end
end

file_editor = FileEditor.new()

file_editor.edit_config('def_col_add', columt_to_add) unless columt_to_add.empty?
file_editor.edit_config('def_col_rm', columt_to_rm) unless columt_to_rm.empty?
file_editor.edit_config('sort_dir', sort_direction) unless sort_direction.empty?
file_editor.edit_config('sort_by', sort_value) unless sort_value.empty?

if search && !help
  result = Searcher.new(search_command, from_file).search
  if result.empty?
    puts 'Issues not found'
  else
    viewer = Viewer.new(result)
    if to_file.empty?
      viewer.to_console
    else
      viewer.to_file(to_file)
      puts "Result added to file #{to_file}"
    end
  end
end

if daily_task && !help
  if author.empty?
    puts "ERROR: Missing params --author"
  elsif issue_id.empty?
    puts "ERROR: Missing params --number"
  else
    command = "created_on = #{date} AND author = #{author} OR comments_created_on = #{date} AND comments_author = #{author}"
    result = Searcher.new(command, from_file).search(date, slow_grab)
    CommentCreator.new(date, result).add(issue_id)
  end
end